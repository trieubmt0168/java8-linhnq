package com.digidinos.shopping.repository;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

import com.digidinos.shopping.dao.AccountDAO;
import com.digidinos.shopping.entity.Account;
import com.digidinos.shopping.util.HibernateUtils;

public class AccountRepository implements AccountDAO{

	@Override
	public List<Account> getAlls() {
		   List<Account> list = null;
	        Session session = HibernateUtils.getSessionFactory().getCurrentSession();
	        try {
	            session.beginTransaction();
	            String hql = "FROM Account  ";
	            Query query = session.createQuery(hql);
	            list = query.list();
	        } catch (Exception e) {
	            e.printStackTrace();
	        } finally {
	            session.close();
	        }
	        return list;
	}

	@Override
	public Account login(String username, String password) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean insert(Account account) {
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        try {
            session.beginTransaction();
            session.save(account);
            session.getTransaction().commit();
            return true;
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        }
        return false;
	}
	@Override
	public boolean update(Account account) {
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        try {
            session.beginTransaction();
            session.update(account);
            session.getTransaction().commit();
            return true;
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        }
        return false;
	}

	@Override
	public boolean delete(int idAccount) {
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
        try {
            session.beginTransaction();
            session.delete(idAccount);
            session.getTransaction().commit();
            return true;
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        }
        return false;
	}

	@Override
	public Account getAccountById(int idAccount) {
		// TODO Auto-generated method stub
		return null;
	}

}
