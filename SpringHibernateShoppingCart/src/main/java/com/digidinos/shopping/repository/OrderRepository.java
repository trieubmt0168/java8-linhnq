package com.digidinos.shopping.repository;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

import com.digidinos.shopping.dao.OrderDAO;
import com.digidinos.shopping.entity.Order;
import com.digidinos.shopping.util.HibernateUtils;

public class OrderRepository implements OrderDAO {

	@Override
	public List<Order> getAlls() {
		List<Order> list = null;
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			String hql = "FROM Order";
			Query query = session.createQuery(hql);
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return list;
	}

	@Override
	public boolean insert(Order order) {
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			session.save(order);
			session.getTransaction().commit();
			return true;
		} catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean update(Order order) {
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			session.update(order);
			session.getTransaction().commit();
			return true;
		} catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean delete(Order order) {
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			session.delete(order);
			session.getTransaction().commit();
			return true;
		} catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public long getOrderById(int id) {
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		long amountcount;
		try {
			session.beginTransaction();
			String hql = "SELECT COUNT(id) FROM Product WHERE id=" + id;
			Query query = session.createQuery(hql);
			Iterator count = query.iterate();
			amountcount = (long) count.next();
			return amountcount;
		} catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
		}
		return 0;
	}
}
