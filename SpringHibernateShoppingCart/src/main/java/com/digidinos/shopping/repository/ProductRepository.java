package com.digidinos.shopping.repository;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

import com.digidinos.shopping.dao.ProductDAO;
import com.digidinos.shopping.entity.Product;
import com.digidinos.shopping.util.HibernateUtils;

public class ProductRepository implements ProductDAO {

	@Override
	public List<Product> getAlls() {
		List<Product> list = null;
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			String hql = "FROM Product";
			Query query = session.createQuery(hql);
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return list;
	}

	@Override
	public boolean insert(Product product) {
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			session.save(product);
			session.getTransaction().commit();
			return true;
		} catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean update(Product product) {
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			session.update(product);
			session.getTransaction().commit();
			return true;
		} catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean delete(Product product) {
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			session.delete(product);
			session.getTransaction().commit();
			return true;
		} catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public long getProductById(int idProduct) {
		Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		long amountcount;
		try {
			session.beginTransaction();
			String hql = "SELECT COUNT(Code) FROM Product WHERE Code=" + idProduct;
			Query query = session.createQuery(hql);
			Iterator count = query.iterate();
			amountcount = (long) count.next();
			return amountcount;
		} catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
		}
		return 0;
	}

}
