package com.digidinos.shopping.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String HomeChomer() {
		return "trangchu";
	}
	@RequestMapping("/admin/account")
	public String Account() {
		return "AdminAccount";
	}
	@RequestMapping("/admin/product")
	public String Product() {
		return "AdminProduct";
	}
	@RequestMapping("/admin/order")
	public String Order() {
		return "AdminOrder";
	}
	@RequestMapping(value="/login", method = RequestMethod.GET)
	public String Login() {
		return "login";
	}
}
