package com.digidinos.shopping.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtils {
	private static final SessionFactory sessionFactory;

    static {
        try {
//            ClassLoader classLoader = HibernateUtils.class.getClassLoader();
//            File file = new File(classLoader.getResource("Hibernate.cfg.xml").getFile());
//            System.out.println("File File File"+file.getAbsolutePath());
            sessionFactory = new Configuration().configure("Hibernate.cfg.xml").buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
