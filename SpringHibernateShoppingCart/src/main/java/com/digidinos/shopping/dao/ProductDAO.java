package com.digidinos.shopping.dao;

import java.util.List;

import com.digidinos.shopping.entity.Product;

public interface ProductDAO {
	public List<Product>  getAlls();
	
	public boolean insert(Product product);
	
	public boolean update(Product product);
	
	public boolean delete(Product product);
	
	public long getProductById(int idProduct);
	
}
