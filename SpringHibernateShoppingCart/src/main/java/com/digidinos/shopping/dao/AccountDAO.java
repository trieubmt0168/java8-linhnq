package com.digidinos.shopping.dao;

import java.util.List;

import com.digidinos.shopping.entity.Account;

public interface AccountDAO {
	public List<Account> getAlls();

    public Account login(String username, String password);

    public boolean insert(Account account);

    public boolean update(Account account);

    public boolean delete(int idAccount);

    public Account getAccountById(int idAccount);
}
