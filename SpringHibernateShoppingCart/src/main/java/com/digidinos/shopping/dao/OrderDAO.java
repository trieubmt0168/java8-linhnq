package com.digidinos.shopping.dao;

import java.util.List;

import com.digidinos.shopping.entity.Order;



public interface OrderDAO {
public List<Order>  getAlls();
	
	public boolean insert(Order order);
	
	public boolean update(Order order);
	
	public boolean delete(Order order);
	
	public long getOrderById(int idOrder);
}
